require([
  'shared/settings_listener'
], function(exports, TorListener) {
  'use strict';

  var Tor = {
    init: function() {
      this._mozSettings = window.navigator.mozSettings;
      if (!this._mozSettings) {
        return;
      }

      this._watchChanges();
    },

    _setAllElements: function() {
      var elementsId = [
        'tor-desc'
      ];
      var toCamelCase = function toCamelCase(str) {
        return str.replace(/\-(.)/g, function(str, p1) {
          return p1.toUpperCase();
        });
      };
      elementsId.forEach(function loopElement(name) {
        this[toCamelCase(name)] =
          document.getElementById(name);
      }, this);
    },

    _watchChanges: function() {
      var localize = navigator.mozL10n.localize;

      SettingsListener.observe('tor.enable', false,
        function onTorEnabledChange(enabled) {
          localize(this.torDesc, enabled ? 'enabled' : 'disabled');
        }.bind(this));
    }
  };

  navigator.mozL10n.once(ScreenLock.init.bind(Tor));
}.bind(null, window));

